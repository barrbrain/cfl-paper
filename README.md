# Chroma from Luma in AV1 Conference Paper

[Download the paper]
(https://gitlab.com/barrbrain/cfl-paper/builds/artifacts/master/raw/build/cfl_paper.pdf?job=build)

[Download draft-egge-netvc-cfl-01]
(https://gitlab.com/barrbrain/cfl-paper/builds/artifacts/master/raw/build/draft-egge-netvc-cfl-01.txt?job=build)

[![pipeline status](https://gitlab.com/barrbrain/cfl-paper/badges/master/pipeline.svg)](https://gitlab.com/barrbrain/cfl-paper/commits/master)


## Requirements
  * latexmk
  * lualaTeX
  * kramdown-rfc2629
  * xml2rfc


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Chroma from Luma in AV1</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/trud/cfl-paper" property="cc:attributionName" rel="cc:attributionURL">Luc Trudeau</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
