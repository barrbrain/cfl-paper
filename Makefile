BUILD = build/
DOC_NAME = cfl_paper
XML_FILE = ${BUILD}draft-egge-netvc-cfl-01.xml

LATEXMK = latexmk -pdflatex=lualatex -jobname=${BUILD}${DOC_NAME} -pdf

# Targets
all: pdf txt

pdf:
	$(LATEXMK) icip.tex

txt:
	kramdown-rfc2629  main.mkd > ${XML_FILE}
	xml2rfc ${XML_FILE}
